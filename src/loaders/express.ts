import express from 'express';
import bodyParser from 'body-parser';
import Keycloak from 'keycloak-connect';
import routes from '../routes';

export default ({ app }: { app: express.Application }) => {
  // CORS
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', '*');
    next();
  });

  // Transforms the raw string of req.body into json
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
  );

  const keycloak = new Keycloak({});

  // app.use(keycloak.middleware({}));

  app.use(
    keycloak.middleware({
      logout: '/logout',
      admin: '/',
    }),
  );

  // Load API routes
  app.use('', routes(keycloak));

  // Error handling
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
      },
    });
  });
};
