import expressLoader from './express';
import logger from './logger';

export default async ({ expressApp }) => {
  await expressLoader({ app: expressApp });
  logger.info('Express loaded.');
};
