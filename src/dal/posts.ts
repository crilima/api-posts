import { Pool } from 'pg';
import config from '../config';

const pool = new Pool({
  user: config.db.user,
  host: config.db.host,
  database: config.db.name,
  password: config.db.password,
  port: config.db.port,
});

const getPosts = () => {
  return pool.query(
    `SELECT ${config.db.schema}."Post"."id", ${config.db.schema}."Post"."content", \
    ${config.db.schema}."Post"."createdAt", ${config.db.schema}."Post"."deletedAt", \
    ${config.db.schema}."Image"."imagelink"
    FROM ${config.db.schema}."Post" LEFT JOIN ${config.db.schema}."Image" ON \
    ${config.db.schema}."Post"."id"=${config.db.schema}."Image"."idPost" \
    ORDER BY ${config.db.schema}."Post"."id" ASC`,
  );
};

const getPostById = idPost => {
  return pool.query(
    `SELECT ${config.db.schema}."Post"."id", ${config.db.schema}."Post"."content", \
    ${config.db.schema}."Post"."createdAt", ${config.db.schema}."Post"."deletedAt", \
    ${config.db.schema}."Image"."imagelink"
    FROM ${config.db.schema}."Post" LEFT JOIN ${config.db.schema}."Image" ON \
    ${config.db.schema}."Post"."id"=${config.db.schema}."Image"."idPost" \
    WHERE ${config.db.schema}."Post".id = $1`,
    [idPost],
  );
};

const getPostsByIdUser = idUser => {
  return pool.query(
    `SELECT ${config.db.schema}."Post"."id", ${config.db.schema}."Post"."content", \
    ${config.db.schema}."Post"."createdAt", ${config.db.schema}."Post"."deletedAt", \
    ${config.db.schema}."Image"."imagelink"
    FROM ${config.db.schema}."Post" LEFT JOIN ${config.db.schema}."Image" ON \
    ${config.db.schema}."Post"."id"=${config.db.schema}."Image"."idPost" \
    WHERE ${config.db.schema}."Post"."idUser" = $1`,
    [idUser],
  );
};

const createPost = (idUser, content, imgLink) => {
  return pool
    .query(
      `INSERT INTO ${config.db.schema}."Post" ("content",  "idUser") VALUES ($1, $2) RETURNING id`,
      [content, idUser],
    )
    .then(createdPost => {
      if (imgLink === null) {
        return createdPost.rows[0].id;
      }
      return pool
        .query(`INSERT INTO ${config.db.schema}."Image" ("imagelink", "idPost") VALUES ($1, $2)`, [
          imgLink,
          createdPost.rows[0].id,
        ])
        .then(() => createdPost.rows[0].id);
    });
};

const updatePost = (idPost, content, imgLink) => {
  return pool
    .query(`UPDATE ${config.db.schema}."Post" SET "content" = $1 WHERE id = $2 RETURNING id`, [
      content,
      idPost,
    ])
    .then(createdPost => {
      if (imgLink === null) {
        return createdPost.rows[0].id;
      }
      return pool
        .query(`UPDATE ${config.db.schema}."Image" SET "imagelink" = $1 WHERE "idPost" = $2)`, [
          imgLink,
          createdPost.rows[0].id,
        ])
        .then(() => createdPost.rows[0].id);
    });
};

const deletePost = idPost => {
  return pool
    .query(
      `DELETE FROM ${config.db.schema}."Image" WHERE \
      ${config.db.schema}."Image"."idPost" = $1`,
      [idPost],
    )
    .then(() => {
      return pool.query(
        `DELETE FROM ${config.db.schema}."Post" WHERE ${config.db.schema}."Post".id = $1`,
        [idPost],
      );
    });
};

const getTimeline = idUser => {
  return pool.query(
    `SELECT po."id", po."content", po."createdAt", po."deletedAt", \
    (Select COUNT(*) FROM ${config.db.schema}."Reaction" reaction \
    JOIN ${config.db.schema}."Post" post ON reaction."idPost" = post."id" \
    WHERE reaction."idPost" = po."id") AS "likes", ${config.db.schema}."Image"."imagelink", \
    public."User"."username" FROM ${config.db.schema}."Post" po LEFT JOIN ${config.db.schema}."Image" ON \
    po."id" = ${config.db.schema}."Image"."idPost" JOIN public."User" ON po."idUser" = public."User"."id" WHERE po."idUser" \
    IN (Select ${config.db.schema}."Relationship"."idTargetUser" FROM ${config.db.schema}."Relationship" \
    WHERE ${config.db.schema}."Relationship"."idCurrentUser" = $1 AND \
    ${config.db.schema}."Relationship"."idRelationshipStatus" = \'e3165f83-d56a-4e75-b5e2-80d4d184c079\') \
    ORDER BY po."createdAt" DESC`,
    [idUser],
  );
};

const createComment = (idPost, content) => {
  return pool.query(
    `INSERT INTO ${config.db.schema}."Comment" ("content",  "idPost") VALUES ($1, $2) RETURNING id`,
    [content, idPost],
  );
};

const getCommentsByIdPost = idPost => {
  return pool.query(
    `SELECT co."content", co."createdAt" FROM ${config.db.schema}."Comment" co WHERE co."idPost" = $1`,
    [idPost],
  );
};

const createReaction = (idUser, idPost) => {
  return pool.query(
    `INSERT INTO ${config.db.schema}."Reaction" ("idUser",  "idPost", "idEmote") \
    VALUES ($1, $2, \'2b396131-664a-4cf2-b978-5b606faa2b67\')`,
    [idUser, idPost],
  );
};

export default {
  getPosts,
  getPostById,
  getPostsByIdUser,
  createPost,
  updatePost,
  deletePost,
  getTimeline,
  createComment,
  getCommentsByIdPost,
  createReaction,
};
