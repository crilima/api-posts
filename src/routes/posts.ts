import { Router } from 'express';
import Keycloak from 'keycloak-connect';
import db from '../dal/posts';
import bckt from '../dal/file-upload';
import schemas from '../config/schemas';
import valid from '../loaders/validation';

export default (app: Router, keycloak: Keycloak) => {
  const route = Router();

  route.get('/status', (req, res) => {
    res.status(200).send();
  });

  route.get('/', keycloak.protect(), (req, res, next) => {
    db.getPosts()
      .then(result => res.status(200).json(result.rows))
      .catch(next);
  });

  route.get('/:id', keycloak.protect(), valid(schemas.postID, 'params'), (req, res, next) => {
    const idPost = req.params.id;
    db.getPostById(idPost)
      .then(result => res.status(200).json(result.rows[0]))
      .catch(next);
  });

  route.get('/user/:id', keycloak.protect(), valid(schemas.userID, 'params'), (req, res, next) => {
    const idUser = req.params.id;
    db.getPostsByIdUser(idUser)
      .then(result => res.status(200).json(result.rows))
      .catch(next);
  });

  route.post('/', keycloak.protect(), bckt.upload.single('uploadimg'), (req, res, next) => {
    const { error } = schemas.createPost.validate(req.body);
    if (error != null) {
      const { details } = error;
      const message = details.map(i => i.message).join(',');
      res.status(422).json({ error: message });
    }
    const { content, idUser } = req.body;
    let uploadedImg = null;
    if (req.file !== undefined) {
      uploadedImg = req.file.location;
    }
    db.createPost(idUser, content, uploadedImg)
      .then(result => res.status(201).send(result))
      .catch(next);
  });

  route.put(
    '/:id',
    keycloak.protect(),
    valid(schemas.postID, 'params'),
    bckt.upload.single('uploadimg'),
    (req, res, next) => {
      const { error } = schemas.updatePost.validate(req.body);
      if (error != null) {
        const { details } = error;
        const message = details.map(i => i.message).join(',');
        res.status(422).json({ error: message });
      }
      const idPost = req.params.id;
      const { content } = req.body;
      let uploadedImg = null;
      if (req.file !== undefined) {
        uploadedImg = req.file.location;
      }
      db.updatePost(idPost, content, uploadedImg)
        .then(result => res.status(200).send())
        .catch(next);
    },
  );

  route.delete('/:id', keycloak.protect(), valid(schemas.postID, 'params'), (req, res, next) => {
    const idPost = req.params.id;
    db.deletePost(idPost)
      .then(result => res.status(200).send())
      .catch(next);
  });

  route.get(
    '/timeline/:id',
    keycloak.protect(),
    valid(schemas.userID, 'params'),
    (req, res, next) => {
      const idUser = req.params.id;
      db.getTimeline(idUser)
        .then(result => res.status(200).json(result.rows))
        .catch(next);
    },
  );

  route.post('/comment', keycloak.protect(), valid(schemas.comment, 'body'), (req, res, next) => {
    const { content, idPost } = req.body;
    db.createComment(idPost, content)
      .then(result => res.status(201).send(result.rows[0].id))
      .catch(next);
  });

  route.get(
    '/postcomment/:id',
    keycloak.protect(),
    valid(schemas.postID, 'params'),
    (req, res, next) => {
      const idPost = req.params.id;
      db.getCommentsByIdPost(idPost)
        .then(result => res.status(200).json(result.rows))
        .catch(next);
    },
  );

  route.post('/reaction', keycloak.protect(), valid(schemas.reaction, 'body'), (req, res, next) => {
    const { idPost, idUser } = req.body;
    db.createReaction(idUser, idPost)
      .then(result => res.status(200).send())
      .catch(next);
  });

  app.use('/posts', route);
};
