import { Router } from 'express';
import posts from './posts';

export default keycloak => {
  const app = Router();
  posts(app, keycloak);
  return app;
};
