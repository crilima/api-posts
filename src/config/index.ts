import dotenv from 'dotenv';

const envFound = dotenv.config();
if (!envFound) {
  throw new Error('Could not find .env file !');
}

export default {
  port: process.env.PORT || 5001,
  env: process.env.NODE_ENV || 'production',
  db: {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    name: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA,
  },
  bucket: {
    accessKey: process.env.ACCESS_KEY,
    secretKey: process.env.SECRET_KEY,
    provider: process.env.BUCKET_PROVIDER,
    endpoint: process.env.BUCKET_ENDPOINT,
    region: process.env.BUCKET_REGION,
    name: process.env.BUCKET_NAME,
  },
  logs: {
    level: process.env.LOG_LEVEL || 'info',
  },
};
