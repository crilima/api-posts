#!/bin/bash

if [ "$1" = "" ]; then
  printf 'The name of the docker image is missing ($1)'
  exit 1
fi
if [ "$2" = "" ]; then
  printf 'The path to the Dockerfile is missing ($2)'
  exit 1
fi

printf 'Building the server...'
npm install && npm audit fix
npm run build || return 1

docker build -t $1 $2
